from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}

    url = "https://api.pexels.com/v1/search"

    params = {
        "per_page": 1,
        "query": city + " " + state,
    }

    response = requests.get(url, params=params, headers=headers)

    # converting our response back into python
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except Exception:
        return {"picture_url": None}


def get_weather_data(city,state):

    url = "http://api.openweathermap.org/geo/1.0/direct"

    params = {
        "q": f"{city},{state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit" : 1
    }

    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    url = "https://api.openweathermap.org/data/2.5/weather"

    params = {
        "lat" : latitude,
        "lon" : longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
            }
    except (KeyError, IndexError):
        return None
